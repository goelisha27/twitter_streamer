from django.conf.urls import patterns, include, url
from django.contrib import admin

from sitebase.views import IndexView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'twitter_streamer.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^admin/', include(admin.site.urls)),
)
