# Twitter-Streamer

## 1. Download Code from the repository.
### git clone git@bitbucket.org:goelisha27/twitter_streamer.git

## 2. Install the Requirements
### pip install -r requirements.txt

## 3. Run the code on the local server
### python manage.py runserver

## 4. Run the application on web browser at http://localhost:8000
