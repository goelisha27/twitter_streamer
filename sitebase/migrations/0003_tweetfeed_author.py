# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sitebase', '0002_hashtags'),
    ]

    operations = [
        migrations.AddField(
            model_name='tweetfeed',
            name='author',
            field=models.CharField(default='goelisha27', max_length=200),
            preserve_default=False,
        ),
    ]
