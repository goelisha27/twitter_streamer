# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TweetFeed',
            fields=[
                ('text', models.CharField(max_length=200)),
                ('id', models.BigIntegerField(serialize=False, primary_key=True, db_index=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
