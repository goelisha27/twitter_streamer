from django.contrib import admin
from .models import Hashtags, TweetFeed
# Register your models here.

admin.site.register(Hashtags)
admin.site.register(TweetFeed)
