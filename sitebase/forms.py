from django import forms


class HashTagForm(forms.Form):
    name = forms.CharField(label='Enter Hash Tag', required=False)