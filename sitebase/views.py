import tweepy
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.db.models import Count

from .forms import HashTagForm
from .models import TweetFeed, Hashtags

# Create your views here.


consumer_key = 'ifDxW00IwpgJFx9LvHKf9LXF5'
consumer_secret = 'trmN7i93vTYFsPMvpLFx4Yl1mK6CGzh8OcXMsdX3elcba6Kint'

access_token = '2915170573-1c6w3X6quXEoPpp3LLppXYav7WbZzDWqS3rlx7w'
access_token_secret = 'jBW52rEuzJN8L4HZf9vNkpBu5pFwItA56u1PVJr6rJbgl'

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)


class MyStreamListener(tweepy.StreamListener):
    def on_status(self, status):
        TweetFeed.objects.get_or_create(text=status.text, id=status.id, author=status.author.screen_name)


myStreamListener = MyStreamListener()
myStream = tweepy.Stream(auth=api.auth, listener=myStreamListener)
all_tags = Hashtags.objects.values_list('text', flat=True).all()
myStream.filter(track=all_tags, async=True)


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['cur_tags'] = ", ".join(Hashtags.objects.values_list('text', flat=True).all())
        context['total_tweet'] = TweetFeed.objects.all().count()
        context['total_user'] = TweetFeed.objects.values('author').distinct().count()
        context['max_user'] = TweetFeed.objects.values('author').annotate(tweets=Count('author')).order_by('-tweets')[0]
        context['form'] = HashTagForm()
        return context

    def post(self, request, *args, **kwargs):
        res = 'Failed.. Please try again'

        if request.POST.get('name'):
            Hashtags.objects.create(text=request.POST.get('name'))
            myStream.disconnect()
            all_tags = Hashtags.objects.values_list('text', flat=True).all()
            myStream.filter(track=all_tags, async=True)
            res = 'Hash tag added successfully.'
        return HttpResponse(res)
