from django.db import models

# Create your models here.


class Hashtags(models.Model):
    text = models.CharField(max_length=200)


class TweetFeed(models.Model):
    text = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    id = models.BigIntegerField(db_index=True, primary_key=True)
